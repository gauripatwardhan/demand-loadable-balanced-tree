#pragma once

#include <folly/io/IOBuf.h>

#include <iostream>
#include <memory>
#include <string>

class Value {
public:
	Value();

	explicit Value(folly::IOBuf&& buffer);

	~Value();

	Value(const Value&) = delete;
	Value& operator=(const Value&) = delete;

	Value(Value&&);
	Value& operator=(Value&&);

	bool operator==(const Value& other) const {
		return folly::IOBufEqualTo()(buffer, other.buffer);
	}

	bool operator!=(const Value& other) const {
		return ! operator==(other);
	}

	Value shallowCopy() const {
		if (!isEmpty()) {
			return Value(buffer.cloneAsValue());
		}
		return Value();
	}

	bool isEmpty() const {
		return buffer.empty();
	}

	bool isContiguous() const {
		return !buffer.isChained();
	}

	size_t length() const {
		return buffer.isChained() ? buffer.computeChainDataLength() : buffer.length();
	}

	template<typename T>
	const T* getBufferAs() const {
		if(isContiguous() && length() >= sizeof(T))
			return reinterpret_cast<const T*>(buffer.data());
		throw;
	}

	template<typename T>
	T* getBufferAs() {
		if(isContiguous() && length() >= sizeof(T))
			return reinterpret_cast<const T*>(buffer.writableData());
		throw;
	}

	const folly::IOBuf& getBuffer() const {
		return buffer;
	}

	folly::IOBuf& getBuffer() {
		return buffer;
	}

	std::string asString() const;

	template<typename Archive>
	void serialize(Archive& archive) {
		archive(buffer);
	}

private:
	folly::IOBuf buffer;
};

