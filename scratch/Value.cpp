#include "Value.hpp"
#include <folly/io/IOBuf.h>
#include <folly/io/Cursor.h>
#include <algorithm>

Value::Value() : buffer() {}

Value::Value(folly::IOBuf&& source) : buffer(std::move(source)) {}

Value::~Value() = default;

Value::Value(Value&&) = default;
Value& Value::operator=(Value&&) = default;

std::string Value::asString() const {
	if(buffer.empty())
		return std::string();

	folly::io::Cursor cursor(&buffer);
	return cursor.readFixedString(cursor.totalLength());
}

