
// BalancedTreediskversion.cpp : Defines the entry point for the console application.
//
#pragma once 

#include <assert.h>
#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <fstream>
#include "include/cereal/archives/binary.hpp"
#include "include/cereal/types/string.hpp"
#include "include/cereal/types/memory.hpp"
#include "include/cereal/types/vector.hpp"

class A {
public:
	int a = 0;
};

class B : public A {
public:
	int b = 1;
};

class Blanket : public B {
public:
	float area;
	float perimeter;
	std::string color;
	bool is_velvet;
	bool is_woolen;
	std::string region;

	Blanket() {}

	Blanket (std::string color1, float area1, float perimeter1, std::string region1, bool is_velvet1, bool is_woolen1) :
		color(color1), 
		area(area1), 
		perimeter(perimeter1), 
		region(region1), 
		is_velvet(is_velvet1), 
		is_woolen(is_woolen1) {}
	
	using blanket_p = std::shared_ptr<Blanket>;

	bool operator<(std::shared_ptr<Blanket>& bl) const {
//		std::cout << "checking for lesser \n";
		if (region < bl->region) {
			std::cout << "region smaller \n";
			return true;
		}
		if (region > bl->region) {
			return false;
		}
		if (color < bl->color) {
			return true;
		}
		if (color > bl->color) {
			return false;
		}
		if (area < bl->area) {
			return true;
		}
		if (area > bl->area) {
			return false;
		}
		if (perimeter < bl->perimeter) {
			return true;
		}
		if (perimeter > bl->perimeter) {
			return false;
		}
		if (is_velvet == bl->is_velvet) {
			return false;
		}
		if (is_velvet == false) {
			return true;
		}
		return false;
	}

	~Blanket() {
		std::cout << "destroying \n";
	}
	bool operator>(std::shared_ptr<Blanket>& bl) const {	
//		std::cout << "checking for greateness \n";
		if ((this->operator == (bl)) == false) {
			return !(this->operator <(bl));
		}
		else {
			return false;
		}
	}
	
	bool operator==(std::shared_ptr<Blanket>& bl) const {
//		std::cout << "checking for duplicates \n";
		if (region < bl->region) {
			return false;
		}
		if (region > bl->region) {
			return false;
		}
		if (color < bl->color) {
			return false;
		}
		if (color > bl->color) {
			return false;
		}
		if (area < bl->area) {
			return false;
		}
		if (area > bl->area) {
			return false;
		}
		if (perimeter < bl->perimeter) {
			return false;
		}
		if (perimeter > bl->perimeter) {
			return false;
		}
		if (is_velvet == bl->is_velvet) {
			assert(is_woolen == bl->is_woolen); 
			return true;
		}
		return false;
	}
	 void print_object() {                 
		std::cout << "region : " << region << "  ";                 
		std::cout << "color : " << color << "  ";                 
		std::cout << "area : " << area << " ";                 
		std::cout << "perimeter : " << perimeter << "  ";                 
		std::cout << "velvet : " << is_velvet << "  ";                 
		std::cout << "woolen : " << is_woolen << "  ";                 
		std::cout << '\n';         
	}
	
	template<typename Archive>
	void save(Archive &archive) const {
		archive( cereal::make_size_tag(67) );	
		archive( cereal::make_size_tag(9) );	
	 	archive << region << color << area << perimeter << is_velvet << is_woolen << a << b;
	}
	
	template<typename Archive>
	void load(Archive& archive) {
		std::cout << "Invoked load of Blanket class\n";
		int h = 0;
		archive( cereal::make_size_tag(h) );
		std::cout << h << "\n";
		int j = 0;
		archive( cereal::make_size_tag(j) );
		std::cout << j << "\n";
	 	archive >> region >> color >> area >> perimeter >> is_velvet >> is_woolen >> a >> b;
		std::cout << "Returning from load of Blanket class\n";
	}
};

using blanket_p = std::shared_ptr<Blanket>;

blanket_p create_blanket(int seed) {
	std::vector<std::string> colors{ "red", "green", "blue", "yellow", "gray", "black", "white", "orange", "pink", "purple",
		"violet", "skin", "brown", "maroon", "silver" };//15
	std::vector<float> size{ 300, 609, 301, 526,  678, 786, 234, 901, 1067, 986, 689, 123, 235, 684, 921};//15
	std::vector<float> perimeters{ 60, 90, 100, 121, 45, 123, 78, 103, 78, 91, 65, 56, 12, 21, 54};//15
	std::vector<std::string> places{ "kashmir", "germany", "us", "bharat", "china", "haiti", "hawai", "mexico", "korea", "canada",
 		"puerto rico", "hololulu", "russia", "spain", "japan" };
	std::vector<bool> cloth{ true, false };
	int index;
	srand(seed);
	index = rand() % 15; 
	std::string color = colors[index];
	index = rand() % 15;
	float area = size[index];
	index = rand() % 15;
	float perimeter = perimeters[index];
	index = rand() % 15;
	std::string region = places[index];
	index = rand() % 2;
	bool is_velvet = cloth[index];
	bool is_woolen;
	if (index == 0) {
		is_woolen = false;
	}
	else {
		is_woolen = true;
	}
	auto new_b = std::make_shared<Blanket>(color, area, perimeter, region, is_velvet, is_woolen );
	return new_b;
	
}

void creating(std::vector<std::shared_ptr<Blanket>>& bl, int howmany) {         
	for(int i = 0; i <= howmany; i++) {                 
		auto b = create_blanket(i);                 
		bl.emplace_back(b);                 
		std::cout << i << '\n';                 
		bl[i]->print_object();         
	} 
}
