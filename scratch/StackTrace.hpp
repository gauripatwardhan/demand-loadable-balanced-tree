#include <stdio.h>
#include <signal.h>
#include <execinfo.h>
#include <stdlib.h>

constexpr int STACK_SIZE = 256;

void stackTrace() {

  void *trace[STACK_SIZE];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  trace_size = backtrace(trace, STACK_SIZE);
  messages = backtrace_symbols(trace, trace_size);
  /* skip first stack frame (points here) */
  printf("[bt] Execution path:\n");
  for (i=1; i<trace_size; ++i)
  {
    printf("[bt] #%d %s\n", i, messages[i]);
  }

  return;
}
