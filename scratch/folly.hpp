#pragma once

#include <iostream>
#include <algorithm>
#include <string>
#include <type_traits>
#include <streambuf>
#include <memory>
#include <iosfwd>
#include <cstdint>
#include <cstddef>

#include "Value.hpp"
#include "StackTrace.hpp"

#include <folly/io/Cursor.h>
#include <folly/io/IOBuf.h>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/details/traits.hpp>

template<typename OutArchive, size_t DefaultBufferSize>
struct Serializer {
	using InArchive = typename cereal::traits::detail::get_input_from_output<OutArchive>::type;
	
	template<typename T, typename = std::enable_if_t<cereal::traits::is_output_serializable<T, OutArchive>::value>>
	static folly::IOBuf serialize(const T&object, size_t bufferSize = DefaultBufferSize) {
		static_assert(cereal::traits::is_input_serializable<T, InArchive>::value, "Type should be input serializable too");
		
		MemoryOutStreamBuf streambuf(bufferSize);
		std::ostream out(&streambuf);
		{
			OutArchive archive(out);
			archive(object);
		}
		return streambuf.release();
	}
		
	template<typename T, typename = std::enable_if_t<cereal::traits::is_input_serializable<T, InArchive>::value>>
	static void deserialize(const folly::IOBuf& buffer, T& t) {
		static_assert(cereal::traits::is_output_serializable<T, OutArchive>::value, "Type should be output serializable too");

		MemoryInStreamBuf streambuf(folly::io::Cursor{&buffer});
		std::istream in(&streambuf);
		{
			InArchive archive(in);
			archive(t);
		}
	}

	template<typename T, typename = std::enable_if_t<cereal::traits::is_input_serializable<T, InArchive>::value>>
	static T deserialize(const folly::IOBuf& buffer) {
		T deserializedValue;
		deserializer(buffer, deserializedValue);
		return deserializedValue;
	}

	template<typename T, typename = std::enable_if_t<cereal::traits::is_input_serializable<T, InArchive>::value>>
	static T deserialize(const Value& value) {
		return deserialize<T>(value.getBuffer());
	}

	template<typename T, typename = std::enable_if_t<cereal::traits::is_input_serializable<T, InArchive>::value>>
	static void deserialize(const Value& value, T& t) {
		deserialize<T>(value.getBuffer(), t);
	}
	
private:
	
	class MemoryOutStreamBuf : public std::streambuf {
	public: 
		explicit MemoryOutStreamBuf(size_t bufferSize) : bufferSize(bufferSize), head(folly::IOBuf::CREATE, bufferSize) {
			userBuffer(head);
		}

		folly::IOBuf release() {
			append();
			return head;
		}

	private:
		int_type overflow(int_type ch) override {
			append();
			allocate();
			return sputc(ch);
		}

		std::streamsize showmanyc() override {
			return -1;
		}

		void userBuffer(folly::IOBuf& buffer) {
			assert (buffer.capacity() >= bufferSize);
			auto begin = reinterpret_cast<char*>(buffer.writableBuffer());
			auto end = reinterpret_cast<char*>(buffer.writableBuffer() + bufferSize);
			setp(begin, end);
		}

		void append() {
			const std::streamsize length = pptr() - pbase();
			if (head.isChained()) {
				head.prev()->append(length);
			}
			else {
				head.append(length);
			}
		}	

		void allocate() {
			auto iobuf = folly::IOBuf::create(bufferSize);
			userBuffer(*iobuf);
			head.prependChain(std::move(iobuf));
		}
		
		size_t bufferSize;
		folly::IOBuf head;
	};
	
	class MemoryInStreamBuf : public std::streambuf {
	public:
		explicit MemoryInStreamBuf(folly::io::Cursor cursor) : cursor(std::move(cursor)) {
			if(!useNextBuffer()) {
				std::cout << "Created a MemoryInStreambuf with an empty buffer: /n";
			       	stackTrace();
			}
		}
	
	private:
		int_type underflow() override {
			return useNextBuffer() ? sgetc() : traits_type::eof();
		}
		
		bool useNextBuffer() {
			const auto buf = cursor.peekBytes();
			if(buf.empty()) {
				return false;
			}
			cursor.skip(buf.size());
			const auto bufStart = reinterpret_cast<char_type*>(const_cast<unsigned char*>(buf.begin()));
			setg(bufStart, bufStart, bufStart + buf.size());
			return true;
		}

		folly::io::Cursor cursor;
	};
};
	
			
	
		
		
