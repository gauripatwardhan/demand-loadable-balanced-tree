﻿#pragma once

#include <type_traits>
#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <typeinfo>
#include "Blanket.hpp" 
#include "include/cereal/archives/binary.hpp"
#include "include/cereal/types/vector.hpp"
#include "include/cereal/types/memory.hpp"
#include "include/cereal/types/string.hpp"

template <typename T>
class Base : public std::enable_shared_from_this<Base<T>> {
protected:
	using shared_b = std::shared_ptr<Base<T>>;
	int n = 0;							// keeps count of # of elements in val_vector
	std::vector<T> val_vector;

public:
	bool is_child;							// keeps track of type of the node
	int child_max;
	int parent_max;

protected:
	/* destroy base and clear val_vector
	 */
	~Base();
	
	/* default constructor
	 */
	Base();

	/* figure out where val should be inserted in "this" so that the ascending order is preserved. Returns -1 if the element 
	 * already exists, else returns mid.
	 */
	template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type* = nullptr>	
	int get_pos(const std::shared_ptr<Q>& val);
	
	template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type* = nullptr>	
	int get_pos(const Q* val);

	int get_pos(const std::string& val);

	template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type* = nullptr>	
	int get_pos(const Q& val);
	
	template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type* = nullptr>	
	int get_pos(const Q& val);

	/* first checks if old_node is a child if so, it copies all elements at and after pos to a new node and sets *inserted to 
	 * the first element in new node. If old_node is a parent, it sets *insert to the value at pos in val_varray, then it 
	 * copies all the elements after pos into a new node.
	 */
	void split_node(shared_b& new_node, int pos, T& inserted);

	/* constructor creates a new root (if new root is Pnode the pnode contructor will be calling this so no need to supply 
	 * the children to Base constructor)
	 */
	Base(T val, bool child, int child_max1, int parent_max1);

	/* given a Cnode, position, value and a spare node inserts value at poisition in Cnode. If spare node is NULL then it returs. If 
	 * spare node is not NULL it splits the Cnode and stores components of the newly created cnode in the spare note and then returns.
	 */
	int insert(T& val, int pos, std::shared_ptr<Base<T>>& new_child, T& insert);

	/* checks if node has reached max capacity. If it has returns 1, else returns 0.
	 */
	int check_full(int max);
	
	/* if same element is found returns 0. Else returns 1.
	 */
	int is_duplicate(const std::string val);					
	
	template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type* = nullptr>	
	int is_duplicate(const std::shared_ptr<Q>& val);
  
	template<typename Q, typename std::enable_if<std::is_pointer<Q>::value, Q>::type* = nullptr>	
	int is_duplicate(const Q* val); 

	template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type* = nullptr>	
	int is_duplicate(const Q& val);					

	template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type* = nullptr>	
	int is_duplicate(const Q& val);					

	/* save btree in file
	 */
	void base_save(std::string file_name);
	void one_file_save(std::string file_name);
	int save_intoanything(std::ofstream ofs);
};

template<typename T>
Base<T>::Base() {}								// default constructor

template<typename T>
Base<T>::Base(T val, bool child, int child_max1, int parent_max1) { 		// creates a new node (root)
	val_vector.insert(val_vector.begin(), val);
	is_child = child;
	child_max = child_max1;
	parent_max = parent_max1;
	n = 1;
}

template<typename T>
template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type*>	
int Base<T>::get_pos(const Q& val) {						// for generic (default method)
	int s = 0;
	int e = this->n;
	int mid = (e - s) / 2;
	if (this->val_vector.size() == 0 || val < this->val_vector[0]) {	 // if there is no value in "this" or val is the new
			return 0;		 				 // smallest value retunrs 0
	}
	if(val > this->val_vector[this->n - 1]){				 // if val is the greater than the greatest value
		return this->n;							 // in "this" return the lenght of ("this"+1)
	}
	while (s != e && s != e - 1) {
		if (val < this->val_vector[mid]) {				// if val is smaller than value at mid, focuses on 
			e = mid;						// indexes below mid
			mid = ((e - s) / 2) + s;
			continue;
		}
		if (val > this->val_vector[mid]) {				// if val is greater than value at mid, foucese on
			s = mid;						// indexes above mid
			mid = ((e - s) / 2) + s;
			continue;
		}	
		return -1;	 						// val must be equal to value at mid, returns -1
	}		
	if (val > this->val_vector[mid]) {					// if val is greater than value at mid, increases
		mid++;								// mid by one
	}
	if(val == this->val_vector[mid]) {					// if val is identical to the value at mid, it 
		return -1;							// returns -1 i.e. do not add val in the btree
	}
	return mid;								// returns the mid with all the adjustments
}

template<typename T>
template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type*>	
int Base<T>::get_pos(const std::shared_ptr<Q>& val) {				// for smart pointers
	auto ptr = *val;
	int pos = this->get_pos(ptr);
	return pos;
}
	
template<typename T>
template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type*>	
int Base<T>::get_pos(const Q* val) {						// for raw pointers
	auto ptr = *val;
	int pos = this->get_pos(ptr);
	return pos;
}

template<typename T>
int Base<T>::get_pos(const std::string& val) { 					// for string
	int s = 0;
	int e = this->n;
	int mid = (e - s) / 2;
	if (this->val_vector.size() == 0 || val < this->val_vector[0]) {	 
		return 0;							
	}
	if(val > this->val_vector[this->n - 1]){			
		return this->n;							 
	}
	while (s != e && s != e - 1) {
		if (val < this->val_vector[mid]) {				 
			e = mid;
			mid = ((e - s) / 2) + s;
			continue;
		}
		if (val > this->val_vector[mid]) {			
			s = mid;						
			mid = ((e - s) / 2) + s;
			continue;
		}	
		return -1;	 						
	}			
	if (val > this->val_vector[mid]) {					
		mid++;
	}	
	if(val == this->val_vector[mid]) {
		return -1;
	}
	return mid;								
}	

template<typename T>
template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type*>	
int Base<T>::get_pos(const Q& val) {								// for objects
	assert(std::is_class<Q>::value);
	int s = 0;
	int e = this->n;
	int mid = (e - s) / 2;
	if(this->val_vector.size() == 0 || val.operator < (this->val_vector[0])) {
		return 0;
	}
	if(val.operator > (this->val_vector[this->n - 1])) {
		return this->n;
	}
	while(s != e && s != e - 1) {
		if(val.operator < (this->val_vector[mid])) {
			e = mid;
			mid = ((e - s) / 2) + s;
			continue;
		}
		if(val.operator > (this->val_vector[mid])) {
			s = mid;
			mid = ((e - s) / 2) + s;
			continue;
		}
		return -1;
	}
	if(val.operator > (this->val_vector[mid])) {
		mid++;
	}
	return mid;
}
	
template<typename T>
int Base<T>::is_duplicate(const std::string val) {					
	for (int i = 0; i < this->n; i++){					// if storing string
		if(this->val_vector[i] == val){
			std::cout << this->val_vector[i] << '\n';
			return 0;
		}
	}	
	return 1;
}
	
template<typename T>
template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type*>	
int Base<T>::is_duplicate(const std::shared_ptr<Q>& val) {			// if storing smart pointers 
	assert(false == std::is_pointer<Q>::value);				// NOTE: if storing shared_ptr<some class> and the 
	int exist = this->is_duplicate(*val);					// class is user defined, it MUST have ==, <, >  
	return exist;								// opeator methods with shared_ptr<some class>& as
}										// the sole argument
 
template<typename T>
template<typename Q, typename std::enable_if<std::is_pointer<Q>::value, Q>::type*>	
int Base<T>::is_duplicate(const Q* val) {					// if storing raw_pointers
	assert(std::is_pointer<Q>::value == true);
	int exist = this->duplicate(*val);
	return exist;
}

template<typename T>
template<typename Q, typename std::enable_if<std::is_class<Q>::value, Q>::type*>	
int Base<T>::is_duplicate(const Q& val) {					
	assert(std::is_class<Q>::value == true);				// if storing objects
	for (int i = 0; i < this->n; i++){					// User MUST have defined <, >, == operator methods
		bool exist = val.operator == (this->val_vector[i]);		// within the class with a sole argument whose type is 
		if(exist) {							// is the same at the type of the object being stored. 
			return 0;						// Each method MUST return a bool value only; either true 
		}								// if the condition is met, else false
	}
	return 1;
}

template<typename T>
template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type*>	
int Base<T>::is_duplicate(const Q& val) {					
	for (int i = 0; i < this->n; i++){					// if storing standard types
		if(this->val_vector[i] == val){
			std::cout << this->val_vector[i] << '\n';
			return 0;
		}
	}
	return 1;
}

template<typename T>
int Base<T>::check_full(int max) {						// if number of elements in vector equals or exceeds
	if(this->n >= max) {							// maximum capacity for that node type, retuns 1;
		return 1;
	}
	return 0;
}

template<typename T>
void Base<T>::split_node(shared_b& new_node, int pos, T& inserted) {		// copies values ahead of pos into new_node
	int j = 0;
	if (this->is_child == true) {
		for (int i = pos; i <= this->child_max; i++) {
			new_node->val_vector.insert(new_node->val_vector.begin() + j, this->val_vector[pos]);
			this->val_vector.erase(this->val_vector.begin() + pos);
			std::cout <<" one val is shifted \n";
			this->n--;
			new_node->n++;
			j++;
		}
		inserted = new_node->val_vector[0];
		new_node->child_max = this->child_max;
		new_node->parent_max = this->parent_max;
		new_node->is_child = this->is_child;
		return;
	}	
	assert(this->is_child == false);
	inserted = this->val_vector[pos];
	this->val_vector.erase(this->val_vector.begin() + pos);
	this->n--;
	new_node->is_child = this->is_child;
	for (int i = (pos + 1); i < this->n; i++) {
		new_node->val_vector[new_node->n] = this->val_vector[pos + 1];
		this->val_vector.erase(this->val_vector.begin() + pos + 1);
		this->n--;
		new_node->n++;
	}
}

template<typename T>
int Base<T>::insert(T& val, int pos, std::shared_ptr<Base<T>>& new_child, T& insert) {
	if (new_child == NULL) {						// if Cnode is not to be split, inserts val at pos
		this->val_vector.insert(val_vector.begin() + pos, val);
		this->n++;
		return 0;
	}
	assert(new_child != NULL);						// if Cnode is to be split, inserts val at pos and
	this->val_vector.insert(val_vector.begin() + pos, val);
	this->n++;
	int position = this->n / 2;
	T insert_in_parent;
	this->split_node(new_child, position, insert_in_parent);
	insert = insert_in_parent;
	return 1;
}

/***************************************************************************	
template<typename T>			
void Base<T>::base_save(std::string file_name) {
	if(this->is_child == true) {
		if(is_smart_pointer<T>::value) {

		}
		if(std::is_class<T>::value) {
			for(int i = 0; i < this->n; i++) {
				this->val_vector.at(i).save(file_name + " Cnode");
			}
			return;
		}
		std::ofstream ofs(file_name + " Cnode");     
		boost::archive::text_oarchive ar(ofs);
		for(int i = 0; i <this->n; i++) {
			ar << this->val_vector.at(i);
		} 
		return;
	}
	for(int i = 0; i <= this->n; i++) {
		std::static_pointer_cast<Pnode<T>>(this->pointer_vector).at(i)->base_save(file_name + " " + i);
	}
	std::ofstream ofs(file_name);     
	boost::archive::text_oarchive ar(ofs);
	if(is_smart_pointer<T>::value) {

	}
	if(std::is_class<T>::value) {
		for(int i = 0; i < this->n; i++) {
			this->val_vector.at(i).save(file_name);
		}
		return;
	}
	for(int i = 0; i < this->n; i++) {
		ar << this->val_vector.at(i);
	}
	ofs.close();
	return;
}

template<typename T>
void Base<T>::save_one_file(std::string file_name) {					// save Btree in one file (in desencding order)
	std::ofstream ofs(file_name);     
	boost::archive::text_oarchive ar(ofs);
	if (ofs.peek() == std::ifstream::traits_type::eof()) { 				// if file is empty, save # of levels, child_max,
		if(this->is_child == true) {						// and parent_max; in that order
			ar << 1;
		}
		else {
			int j = 2;
			if(this->poitner_vector[0]->is_child == true) {
				ar << j;
			}
			else {
				std::shared_ptr<Pnode<T>> child = std::static_pointer_cast<Pnode<T>>(this->pointer_vector[0]);
				j++;
				while(child->pointer_vector[0]->is_child != true) {
					child = child->pointer_vector[0];
					j++;
				}
				ar << j;	
			}
		}
		ar << this->child_max;							// and # of levels in the tree in that order first
		ar << this->parent_max;
	}
	if(is_smart_pointer<T>::value) {						// if std::shared_ptr 
		
	}
	for(int i = 0; i < this->n; i++) {						// save all the elements, use '|' for NULL	
		ar << this->val_vector.at(i);
	}
	if(this->is_child == true) {
		assert(this->n <= this->child_max);
		if(this->n < this->child_max) {
			for(int i = this->n; i <= this->child_max; i++) {
				ar << "|";
			}
		return;	
	}
	assert(this->is_child != true);
	assert(this->n <= this->parent_max);
	if(this->n < this->parent_max) {
		for(int i = this->n; i <= this->parent_max; i++) {
			ar << "|";
		}
	}
	for(int i = this->n; i >= 0; i--) {
		this->pointer_vector.at(i)->save_one_file(file_name);
	} 
	return;
}

template<typename T>
int Base<T>::save_intoanything(std::ofstearm ofs){
	if(ofs.open) {
		cereal::BinaryOutputArchive sr(ofs);
		if (this->is_child == true) {
			int starting_position = ofs.tellp();
			sr(this->val_vector);
			return starting_position;
		}
		assert(this->is_child == false);
		for(int i = 0; i <= this->n; i++) {
			int start = this->pointer_vector[i]->save_intoanything(ofs);
			this->has_child.insert(has_child.begin() + i, true);
			this->offsets.insert(offsets.begin() + i, start);
			bool type = this->pointer_vector[i]->is_child;
			this->child_type.insert(child_type.begin() + i, type);
		}
		for (int i = 1 + this->n; i <= this->parent_max; i++) {
			this->has_child.insert(has_child.begin() + i, false);
			this->offsets.insert(offsets.begin() + i, -1);
			this->child_type.insert(child_type.begin() + i, -1);
		}	
		int starting_position = ofs.tellp();
		sr(this->val_vector);
		sr(this->has_child);
		sr(this->offsets);
		return starting_position;
	}
	std::cout << "Cannot open the file \n";
	return -1;
}
***************************************************************************/	

template<typename T>
Base<T>::~Base() {
	std::cout << "destroying base and freeing up vectors \n";
}
