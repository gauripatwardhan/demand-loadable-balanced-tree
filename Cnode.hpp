#pragma once

#include <memory>
#include <vector>
#include <iostream>
#include "Base.hpp"
#include "Blanket.hpp"
#include <type_traits>
#include <string>

template<typename T>
class Cnode : public Base<T>, std::enable_shared_from_this<Cnode<T>> {
public:
	std::shared_ptr<Cnode<T>> forward = NULL;
	std::shared_ptr<Cnode<T>> backward = NULL;
public:
	/* destroy Cnode
	 */
	~Cnode();
	
	/* default constructor
	 */
	Cnode();
	
	/* Constructors root for the first time only, call base constructor
	 */
	Cnode(T& val, int child_max, int parent_max);

	/* calls is_duplicate. If return true, then returns 0. Elses, checks if full, if not calls get_pos and then insert (Base) wih val, 
	 * NULL and NULL. If full, it creates a new cnode and passess it as an argument to insert (Base).
	 */
	bool cinsert(T& val, std::shared_ptr<Cnode<T>>& newcnode); 

	/*prints all values in given cnode, then calls the function again on this->forward. If this->forward == NULL, it returns.
	 */

	template<typename Q = T, typename std::enable_if<!std::is_class<Q>::value, Q>::type* = nullptr>
	void print_cnodes();		         		

	template<typename Q = T, 
		typename std::enable_if<std::is_class<Q>::value && !std::is_same<Q, std::string>::value, Q>::type* = nullptr>
	void print_cnodes();

	template<typename Q = T, typename std::enable_if<std::is_pointer<Q>::value, Q>::type* = nullptr>
	void print_cnodes();

	template<typename Q = T, typename std::enable_if<std::is_same<Q, std::string>::value, Q>::type* = nullptr>
	void print_cnodes();

	/* save btree if it only has one Cnode root
	 */
	template<typename Archive>
	int save(Archive ofs);

	/* if object stored in the btree is a shared_ptr of a different class call the method below which calls the print method of that
	 * class
	 */
	//void print_cnodes(const std::shared_ptr<Cnode<T>>& val);
};

template<typename T>
Cnode<T>::~Cnode(){
	std::cout << "destroying Cnode \n";
}

template<typename T>						// when creating a Cnode root it uses this
Cnode<T>::Cnode(T& val, int child_max, int parent_max) : Base<T>::Base(val, true, child_max, parent_max){}

template<typename T>
Cnode<T>::Cnode() {}						// default constructor

template<typename T>
bool Cnode<T>::cinsert(T& val, std::shared_ptr<Cnode<T>>& newcnode){
	int exists = Base<T>::is_duplicate(val);		// checks it the element already exists in that node
	if (exists == 0) {
		return false;
	}
	assert(exists == 1);					// it element doesn't exist, checks where to insert it
	int pos = Base<T>::get_pos(val);
	assert(pos != -1);
	int full = Base<T>::check_full(this->child_max);		// checks is the current node has reached its maximum capacity
	std::cout << "full : " << full << '\n';
	if(full == 0){						// if it hasn't calls insert in Base and tells it to not split the node
//		std::cout << "not full \n";
		std::shared_ptr<Base<T>> empty = std::shared_ptr<Base<T>>();
		T tobe_null;
		int split = Base<T>::insert(val, pos, empty, tobe_null);
		/*****************************************
		for(int i = 0; i < this->n; i++) {
			std::cout << "   " << this->val_vector[i] ;
		}
		std::cout << '\n' << this->val_vector[pos] << '\n';
		*******************************************/
		assert(split == 0);
//		std::cout << "you are killing me \n";
		return true;
	}
//	std::cout << "node has to split \n";			// if max is reached tells insert in Base to split the node
	assert(full == 1);
	std::shared_ptr<Base<T>> nchild = std::make_shared<Cnode<T>>();
	T insert;
	int spilt  = Base<T>::insert(val, pos, nchild, insert);
	/***************************************
	for(int i = 0; i < this->n; i++) {
		std::cout << "   " << this->val_vector[i] ;
	}
	std::cout << "\n new node \n";
	for(int i = 0; i < nchild->n; i++) {
		std::cout << "   " << nchild->val_vector[i] ;
	}
	std::cout << '\n';
	****************************************/
	assert(spilt == 1);
	val = insert;
//	std::cout << "okay adjusting val \n";
	newcnode = std::static_pointer_cast<Cnode<T>>(nchild);	// returns the newly created node
	if(this->forward != NULL) {				// if this had a cnode it was pointing to it then it reassigns the forward
		this->forward->backward = newcnode;		// and backward of newcnode and the exisitnig one accordingly.
		newcnode->forward = this->forward;
	}
	else {							// if it wasn't pointing to anything, then sets newcnode->forward to NULL
		newcnode->forward = NULL;

	}	
	this->forward = newcnode;
//	std::cout << "returning after Cnode split \n";
	return true;
}

/***********************************************************************************************************************
template<typename T, typename std::enable_if<std::is_pointer<T>::value, T>::type* = nullptr>
void Cnode<T>::print_cnodes(const T& val){
	std::cout << "using print_cnodes for pointers \n";
	for(int i = 0; i < this->n; i++) {
		this->val_vector[i]->print_object();
	}
	if(this->forward == NULL){
		exit(1);
	}
	this->forward->print_cnodes(first_val);
}

template<typename T, typename std::enable_if<std::is_class<T>::value, T>::type* = nullptr> 
void Cnode<T>::print_cnodes(const T& val) {			
	std::cout << "using print_cnodes for class \n";
	for(int i  = 0; i < this->n; i++) {			
		this->val_vector[i].print_object();
	}
	if(this->forward == NULL) {
		exit(1);
	}
	this->forward->print_cnodes(val);
	return;
}
**************************************************************************************************************************/

template<typename T>
template<typename Q, typename std::enable_if<!std::is_class<Q>::value, Q>::type*>
void Cnode<T>::print_cnodes(){		         		// for generic types
//	std::cout << "using defaullt print_cnodes \n";          // given the smallest value code, it prints its contents and using the
	for(int i = 0; i < this->n; i++) {                 	
		std::cout << this->val_vector[i] << ",   ";         
	}         
	if(this->forward == NULL){                 
		exit(1);         
	}         
	this->forward->print_cnodes(); 
	return;
}

template<typename T>
template<typename Q, typename std::enable_if<std::is_class<Q>::value && !std::is_same<Q, std::string>::value, Q>::type*>
void Cnode<T>::print_cnodes(){		         		// for userdefined types/string 
	if(std::is_same<Q, std::string>::value) {
		for(int i  = 0; i < this->n; i++) {                 
			std::cout << this->val_vector[i] << '\n';         
		}         
		if(this->forward == NULL) {                 
			std::cout <<"so what we got is... \n";
			exit(1);         
		}         
		this->forward->print_cnodes();         
		return; 
	}

	if(std::is_same<Q, std::shared_ptr<Q>>::value)	{
//		std::cout << "using print_cnodes for class \n";         
		for(int i  = 0; i < this->n; i++) {                 
			this->val_vector[i]->print_object();         
		}         
		if(this->forward == NULL) {                 
			std::cout <<"so what we got is... \n";
			exit(1);         
		}         
		this->forward->print_cnodes();         
		return; 
	}

	if(std::is_class<Q>::value && !std::is_same<Q, std::string>::value)	{
//		std::cout << "using print_cnodes for class \n";         
		for(int i  = 0; i < this->n; i++) {                 
			try {
				this->val_vector[i].print_object();
			}
			catch (...) {
				exit(1);
			}         
		}         
		if(this->forward == NULL) {                 
			std::cout <<"so what we got is... \n";
			exit(1);         
		}         
		this->forward->print_cnodes();         
		return; 
	}
}

template<typename T>
template<typename Q, typename std::enable_if<std::is_same<Q, std::string>::value, Q>::type*>
void Cnode<T>::print_cnodes(){		         		// for string 
	if(std::is_same<Q, std::string>::value) {
		for(int i  = 0; i < this->n; i++) {                 
			std::cout << this->val_vector[i] << '\n';         
		}         
		if(this->forward == NULL) {                 
			std::cout <<"so what we got is... \n";
			exit(1);         
		}         
		this->forward->print_cnodes();         
		return; 
	}
}

template<typename T>
template<typename Q, typename std::enable_if<std::is_pointer<Q>::value, Q>::type*>
void Cnode<T>::print_cnodes(){		         		// for pointers 
	if(std::is_pointer<T>::value)	{
		for(int i  = 0; i < this->n; i++) {                 
			std::cout << *(this->val_vector[i]) << '\n';         
		}         
		if(this->forward == NULL) {                 
			std::cout <<"so what we got is... \n";
			exit(1);         
		}         
		this->forward->print_cnodes();         
		return; 
	}
}

template<typename T> template<typename Archive>
int Cnode<T>::save(Archive ofs){
	if(ofs.open) {
		cereal::BinaryOutputArchive sr(ofs);
		assert(this->is_child == true);
		int starting_position = ofs.tellp();
		sr(this->val_vector);
		return starting_position;
	}
	std::cout << "Cannot open the file \n";
	return -1;
}
