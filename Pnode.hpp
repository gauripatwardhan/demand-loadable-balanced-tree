#pragma once

#include <memory>
#include <vector>
#include <iostream>
#include "Base.hpp"
#include "Cnode.hpp"
#include <type_traits>
#include "Blanket.hpp"

template<typename T>
class Pnode : public Base<T>, std::enable_shared_from_this<Pnode<T>> {
public:
	std::vector<std::shared_ptr<Base<T>>> pointer_vector;
	std::vector<bool> has_child;
	std::vector<int> offsets;
	std::vector<int> child_type;
public:
	/* destroy Pnode
	 */
	~Pnode();
	
	/* default constructor
	 */
	Pnode();

	/* construct root for the second time (previout root was a Cnode)
	 */
	Pnode(const T& val, std::shared_ptr<Base<T>> old_node, std::shared_ptr<Base<T>> new_node, int child, int parent);

	/* splits Pnode in two and takes care of the pointer's too
	 */
	void split_pnode(std::shared_ptr<Pnode<T>>& new_node, int pos, T& inserted);

	/* traverses down till it gets Cnode, then calls Cnode's insert. Checks is Cnode returned NULL, if it did returns. Else inserts 
	 * the val and Cnode corretly and then checks is it needs to spilt. If it does it calls split_node and returns the new node. Changes
 	 * val to the value to be inserted in the parent of this Pnode.
	 */
	bool pinsert(T& val, std::shared_ptr<Pnode<T>>& newpnode);
	
	/* traverses down till it gets a Cnode then calls print_cnodes in Cnode class
	 */
	void print_them();

	/* load btree
	 */
	void load_one_file(int read_from, std::shared_ptr<Base<T>> root, std::string file_name, int level);
	void load_intoanything(std::string file_name);
	
	/* save btree
	 */
	template<typename Archive>
	int save(Archive &ofs);
};

template<typename T>
Pnode<T>::~Pnode() {
	std::cout << "destroying Pnode \n";
}

template<typename T>
Pnode<T>::Pnode() {}							// default constructor

template<typename T>							// constructs root
Pnode<T>::Pnode(const T& val, std::shared_ptr<Base<T>> old_node, std::shared_ptr<Base<T>> new_node, int child, int parent) : 
		Base<T>::Base (val, false, child, parent) {
	pointer_vector.insert(pointer_vector.begin(), old_node);
	pointer_vector.insert((pointer_vector.begin() + 1), new_node);
}

template<typename T>
void Pnode<T>::split_pnode(std::shared_ptr<Pnode<T>>& new_node, int pos, T& inserted) {
	int k = 0;							// copies contents of "this" after pos to new_node
	int max = this->n;
	inserted = this->val_vector[pos];
	this->val_vector.erase(this->val_vector.begin() + pos);
	this->n--;
//	std::cout << "pos : " << pos << ", parent_max : " << this->parent_max << '\n';
//	std::cout << "not inserting " << inserted << " in the new pnode \n";
	for(int i = pos; i < this->parent_max; i++) {
//		std::cout << "in here \n";
		new_node->val_vector.insert(new_node->val_vector.begin() + k, this->val_vector[pos]);
		this->val_vector.erase(this->val_vector.begin() + pos);
		this->n = this->n - 1;
		new_node->n = new_node->n + 1;
//		std::cout << "1. this->n : " << this->n << " and new_node->n : " << new_node->n << '\n';
		k++;	
	}
//	std::cout << "this->n : " << this->n << " and new_node->n : " << new_node->n << '\n';
	new_node->parent_max = this->parent_max;
	new_node->child_max = this->child_max;
	new_node->is_child = this->is_child;
	int j = 0;
	for(int i = pos + 1; i <= this->parent_max + 1; i++){
//		std::cout << " i is = " << i << " and max is = " << max << '\n'; 
		new_node->pointer_vector.insert(new_node->pointer_vector.begin() + j, this->pointer_vector[pos + 1]);
		this->pointer_vector.erase(this->pointer_vector.begin() + pos + 1);
		j++;
	}
	return;
}

template<typename T>
bool Pnode<T>::pinsert(T& val, std::shared_ptr<Pnode<T>>& newpnode) {
	//std::cout << val << '\n';
//	std::cout << this << '\n';
//	std::cout << this->pointer_vector.size() << '\n';
//	std::cout << this->val_vector.size() << '\n';
//	std::cout << this->n << '\n';
	int pos = Base<T>::get_pos(val);				// gets here to insert val
//	std::cout << "got pos " << pos <<  "\n";
	if(pos == -1) {	
		return false;
	}				
	if (this->pointer_vector[pos]->is_child == true) {		// if current node is pointing to Cnode calls insert in Cnode
		std::cout << "points to child \n"; 
		std::shared_ptr<Cnode<T>> nchild = std::shared_ptr<Cnode<T>>();
		std::static_pointer_cast<Cnode<T>>(this->pointer_vector[pos])->cinsert(val, nchild);
		if (nchild == NULL) {					// if Cnode didn't split returns
			return true;	
		}
		assert(nchild != NULL);			
//		std::cout << "child split, new val tobe inserted in parent is " << val << '\n';
		nchild->backward = std::static_pointer_cast<Cnode<T>>(this->pointer_vector[pos]);
		int exists = Base<T>::is_duplicate(val);
		assert(exists == 1);
		pos = Base<T>::get_pos(val);				// if Cnode split, figures out where to insert val and nchild.
		int full = Base<T>::check_full(this->parent_max);	
		if (full == 0) {					// if "this" is not full, inserts val and nchild.
			this->val_vector.insert(this->val_vector.begin() + pos, val);
			this->pointer_vector.insert(pointer_vector.begin() + pos + 1, nchild);
			this->n++;
			/******************************************
			std::cout << "after inserting nchild : the first element is : " << this->pointer_vector[pos + 1]->val_vector[0] <<
				'\n';
			std::cout << "element in root are now : ";
			for(int i = 0; i < this->n; i++) {
				std::cout << this->val_vector[i] << ",    ";
			}
			std::cout << '\n';
			*******************************************/
			return true;
		}
		assert(full == 1);					// if "this" also has to split, inserts val and nchild.
		this->val_vector.insert(this->val_vector.begin() + pos, val);
		this->pointer_vector.insert(pointer_vector.begin() + pos + 1, nchild);
		std::cout << "this->n before advance : " << this->n << "\n";
		this->n++;
//		std::cout << "this->n after advance : " << this->n << "\n";
//		std::cout << "Burp, I'm full \n";
		std::shared_ptr<Pnode<T>> pchild = std::make_shared<Pnode<T>> (); 
		T return_this;
//		std::cout << "calling split_pnode \n";
//		std::cout << "this->n when calling split_pnode : " << this->n << "\n";
		this->split_pnode(pchild, this->n/2, return_this);	// calls split_pnode to split "this"
		/*****************************************
		std::cout << "values in pchild : ";
		for (int i = 0; i < pchild->n; i++) {
			std::cout << pchild->val_vector[i] << ",   ";
		}
		std::cout << " \n first element in each child of pchild    ";
		for(int i = 0; i <= pchild->n; i++) {
			std::cout << pchild->pointer_vector[i]->val_vector[0] << ",    ";
		}
		std::cout << '\n';
		*******************************************/
		val = return_this;
		newpnode = pchild;
		return true;
	}
	else {								// if "this" is not pointing to Cnode calls insert in Pnode on it	
		std::cout << "points to Pnode \n";
		std::shared_ptr<Pnode<T>> child = std::shared_ptr<Pnode<T>> ();	
		(std::static_pointer_cast<Pnode<T>>(this->pointer_vector[pos]))->pinsert(val, child);
		if(child == NULL) {					// if any of it's children didnot split it returns
//			std::cout << "none of its children split \n";
			return true;
		}
		assert(child != NULL);					// one of its children split
//		std::cout << "one of its' children split \n";
		int exists = Base<T>::is_duplicate(val);
		assert(exists == 1); 
		pos = Base<T>::get_pos(val);				// figures out where to insert the val and child
		int full = Base<T>::check_full(this->parent_max);	
		if (full == 0) {  					// if "this" is not full, inserts val and child and returns
			this->val_vector.insert(this->val_vector.begin() + pos, val);
			this->pointer_vector.insert(pointer_vector.begin() + pos + 1, child);
			this->n++;          
//			std::cout << "not full \n";
			return true;                 
		}
		assert(full == 1);  					// if "this" is full, repeats the steps mentioned earliers in	
		this->val_vector.insert(this->val_vector.begin() + pos, val); // the "if" loop above
		this->pointer_vector.insert(pointer_vector.begin() + pos + 1, child);
		this->n++;          
//		std::cout << "Burp, I'm full \n"; 
                std::shared_ptr<Pnode<T>> pchild = std::make_shared<Pnode<T>> (); 
                T return_this;                
		this->split_pnode(pchild, this->n/2, return_this);
		/***********************************************
		std::cout << " \n first element in each child of pchild    ";
                for(int i = 0; i <= pchild->n; i++) {    
                	std::cout << pchild->pointer_vector[i]->val_vector[0] << ",    ";  
                }  
                std::cout << '\n';
		*************************************************/
		val = return_this;
		newpnode = pchild;                 
		return true;
	}
}

template<typename T>
void Pnode<T>::print_them() {							// traverses down till it gets the smallest Cnode. Once
	while(this->pointer_vector[0]->is_child	!= true) {			// it finds the smallest cnode, it calls print_cnodes()
		std::static_pointer_cast<Pnode<T>>				// function on it
			(this->pointer_vector[0])->print_them(); 
		return;
	}
	std::static_pointer_cast<Cnode<T>>(this->pointer_vector[0])->print_cnodes();
	return;
}

/************************************************************
template<typename T>			
void Pnode<T>::base_save(std::string file_name) {
	if(this->is_child == true) {
		if(is_smart_pointer<T>::value) {

		}
		if(std::is_class<T>::value) {
			for(int i = 0; i < this->n; i++) {
				this->val_vector.at(i).save(file_name + " Cnode");
			}
			return;
		}
		std::ofstream ofs(file_name + " Cnode");     
		boost::archive::text_oarchive ar(ofs);
		for(int i = 0; i <this->n; i++) {
			ar << this->val_vector.at(i);
		} 
		return;
	}
	for(int i = 0; i <= this->n; i++) {
		this->pointer_vector.at(i)->base_save(file_name + " " + i);
	}
	std::ofstream ofs(file_name);     
	boost::archive::text_oarchive ar(ofs);
	if(is_smart_pointer<T>::value) {

	}
	if(std::is_class<T>::value) {
		for(int i = 0; i < this->n; i++) {
			this->val_vector.at(i).save(file_name);
		}
		return;
	}
	for(int i = 0; i < this->n; i++) {
		ar << this->val_vector.at(i);
	}
	ofs.close();
	return;
}

template<typename T>
bool Pnode<T>::base_load(std::string file_name, std::shared_ptr<Base<T>> node) {
	if(std::ifstream find (file_name)) {
		boost::archive::text_iarchive ar(find);
		const std::size_t pos_end = pos; 
		while (input[pos] == ' ' && pos > 0) --pos;  
		std::string result = input.substr(pos, pos_end - pos);
		if(result == "Cnode") {
			std::shared_ptr<Cnode<T>> cnode = std::make_shared<Cnode<T>>();
			int n = 0;
			T value;
			while (ar >> value)) {
	`			if(is_shared_ptr<T>::value) {
		
				}
				else {
					cnode->val_vector.insert(cnode->val_vector.begin() + n, value);
					n++;
				}
			}
			cnode->n = n;
			cnode->is_child = true;
			node = cnode;
			find.close();
			return true;
		}
		int j = 0;
		std::shared_ptr<Pnode<T>> pnode = std::make_shared<Pnode<T>>();
		int i = 0;
		T value;
		while (ar >> value) {
			if (is_smart_pointer<T>::value) {
				
			} 
			else {
				pnode->val_vector.insert(pnode->val_vector.begin() + i; value);
				i++;
			}
		}
		pnode->n = i;
		pnode->is_child = false;
		while (std::ifstream find (file_name + " " + j)) {
			std::shared_ptr<Base<T>> new_node = std::shared_ptr<Base<T>>();
			this->base_load(file_name + " " + j, new_node);
			assert(new_node != NULL);
			pnode->pointer_vector.pushback(new_node);
			j++;
		}
		while (std::ifstream find (file_name + " " + j + " Cnode")) {
			std::shared_ptr<Base<T>> new_node = std::shared_ptr<Base<T>>();
			this->base_load(file_name + " " + j + " Cnode", new_node);
			assert(new_node != NULL);
			pnode->pointer_vector.pushback(new_node);
			j++;
		}
		find.close();
		assert(pnode->n == j);
		return true;
	}
	std::cout << "something major went wrong when loading \n";
	std::cout << " a file that does exist was called \n";
	std::cout << "the name of the file is : " << file_name << '\n';
	std::cout << "check if the file exists. if it does contact support. \n";
	exit(1);
}	
********************************************************/

/********************************************************
template<typename T>
void Pnode<T>::save_one_file(std::string file_name) {					// save Btree in one file (in desencding order)
	std::ofstream ofs(file_name);     
	boost::archive::text_oarchive ar(ofs);
	if (ofs.peek() == std::ifstream::traits_type::eof()) { 				// if file is empty, save # of levels, child_max,
		if(this->is_child == true) {						// and parent_max; in that order
			ar << 1;
		}
		else {
			int j = 2;
			if(this->poitner_vector[0]->is_child == true) {
				ar << j;
			}
			else {
				std::shared_ptr<Pnode<T>> child = std::static_pointer_cast<Pnode<T>>(this->pointer_vector[0]);
				j++;
				while(child->pointer_vector[0]->is_child != true) {
					child = child->pointer_vector[0];
					j++;
				}
				ar << j;	
			}
		}
		ar << this->child_max;							// and # of levels in the tree in that order first
		ar << this->parent_max;
	}
	if(is_smart_pointer<T>::value) {						// if std::shared_ptr 
		
	}
	for(int i = 0; i < this->n; i++) {						// save all the elements, use '|' for NULL	
		ar << this->val_vector.at(i);
	}
	if(this->is_child == true) {
		assert(this->n <= this->child_max);
		if(this->n < this->child_max) {
			for(int i = this->n; i <= this->child_max; i++) {
				ar << "|";
			}
		return;	
	}
	assert(this->is_child != true);
	assert(this->n <= this->parent_max);
	if(this->n < this->parent_max) {
		for(int i = this->n; i <= this->parent_max; i++) {
			ar << "|";
		}
	}
	for(int i = this->n; i >= 0; i--) {
		this->pointer_vector.at(i)->save_one_file(file_name);
	} 
	return;
}	

void Pnode<T>::load_one_file(int read_from, std::shared_ptr<Base<T>> root, std::string file_name, int level) {
	std::ifstream file(file_name);
	boost::archive::text_iarchive ar(file);
	int levels = ar >> value;
	int child_max = ar >> value;
	int parent_max = ar >> value;
	int k = 3;
	unsigned j;
	if(level == 1) {
		j = 0;
	}
	else {
		j = levels - level - 1;
	}	
	if(root == NULL) {
		assert(read_from == NULL);
		int m = 0;
		if (levels == 1) {
			std::shared_ptr<Cnode<T>> croot = std::shared_ptr<Cnode<T>>;
			while(ar >> value != "|") {
				croot->val_vector.insert(croot->val_vector.begin() + m, value);
				croot->n++;
				m++;
			}
			croot->is_child = true;
			croot->child_max = child_max;
			croot->parent_max = parent_max;
			root = croot;
			return;
		}  	
		assert(levels != 1 && m == 0);
		std::shared_ptr<Pnode<T>> proot = std::shared_ptr<Pnode<T>>;
		while(m < parent_max) {
			if(ar >> value == "|") {
				break;
			}
			proot->val_vector.insert(proot->val_vector.begin() + m, ar >> value);
			proot->n++;
			m++
		}
		proot->is_child = false;
		proot->child_max = child_max;
		proot->parent_max = parent_max;
		root = proot;
		k = k + m;
		
	}
	assert(read_from != NULL);
	while(read_from != 0) {
		ar >> value;
		read_from--;
		k++;
	}
	if(levels > 2) {
		std::shared_ptr<std::shared_ptr<Pnode<T>> node = root;
		for(int i = level; i < levels; i++) {
			std::shared_ptr<Pnode<T>> pnode = std::shared_ptr<Pnode<T>>;
			int m = 0;
			while(m < parent_max) {
				if(ar >> value != "|") {
					pnode->val_vector.insert(pnode->pointer_vector.begin() + m, ar >> value);
					pnode->n++;
				}
				m++;
			}
			k = k + m;
			pnode->is_child = false;
			pnode->child_max = child_max;
			pnode->parent_max = parent_max;
			for(int j = 0; j < i; j++) {
				node = (*node)->pointer_vector.at(0);
			}
			*node->pointer_vector.insert((*node)->pointer_vector.begin(), pnode);
			if(level == 1) {
				j++;
			}
		}
	}
	for(int i = (*node)->n; i >= 0; i++) {
		std::shared_ptr<Cnode<T>> cnode = std::shared_ptr<Cnode<T>>;
		int m = 0;
		while(m < child_max) {
			if(ar >> value != "|") {
				cnode->val_vector.insert(cnode->val_vector.begin + m, ar >> value);
				cnode->n++;
			}
			m++;
		}
		k = k + m;
		cnode->is_child = false;
		cnode->child_max = child_max;
		cnode->parent_max = parent_max;
		(*node)->pointer_vector.insert((*node)->pointer_vector.begin(), cnode);
	}
	this->load_one_file(k + 1, root, file_name, j);
	return;	
}
********************************************************/
/********************************************************
template<typename T>
void Pnode<T>::load_intoanything(std::string file_name) {
	// go to the end of the file
	int position;
	std::string type;
}
********************************************************/


template<typename T>  template<typename Archive>
int Pnode<T>::save(Archive &ofs){
	if(ofs.open) {
		cereal::BinaryOutputArchive sr(ofs);
		if (this->is_child == true) {
			int starting_position = ofs.tellp();
			sr << this->n;
			for(int t = 0; t <= this->n; t++) {
				sr << this->val_vector.at(t);
			}
			return starting_position;
		}
		assert(this->is_child == false);
		for(int i = 0; i <= this->n; i++) {
			int start = this->pointer_vector[i]->save_intoanything(ofs);
			this->has_child.insert(has_child.begin() + i, true);
			this->offsets.insert(offsets.begin() + i, start);
			bool type = this->pointer_vector[i]->is_child;
			this->child_type.insert(child_type.begin() + i, type);
		}
		for (int i = 1 + this->n; i <= this->parent_max; i++) {
			this->has_child.insert(has_child.begin() + i, false);
			this->offsets.insert(offsets.begin() + i, -1);
			this->child_type.insert(child_type.begin() + i, -1);
		}	
		int starting_position = ofs.tellp();
		sr << this->n;
		for(int t = 0; t <= this->n; t++) {
			sr << this->val_vector.at(t);
		}
		for(int t = 0; t <= this->n; t++) {
			sr << this->has_child.at(t);
		}
		for(int t = 0; t <= this->n; t++) {
			sr << this->offsets.at(t);
		}
		for(int t = 0; t <= this->n; t++) {
			sr << this->child_type.at(t);
		}
		return starting_position;
	}
	std::cout << "Cannot open the file \n";
	return -1;
}
