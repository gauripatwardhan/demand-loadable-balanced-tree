﻿#pragma once

#include <iostream>
#include <type_traits>
#include "Base.hpp"
#include "Pnode.hpp"
#include "Cnode.hpp"
#include "Blanket.hpp"
#include <typeinfo>
				
template<typename T>
class Btree {
public:
	using shared_b = std::shared_ptr<Base<T>>;
	std::shared_ptr<Base<T>> root;

public:
	Btree();

	/* destroy btree class
	 */
	~Btree();

	/* checks if root is NULL, if it is casts root to Base and calls the Base constructor with creates a Cnode root. Then it 
	 * returns 1. If root !NULL it casts root to Base and calls its insert (set up ex. shrared_b new_root = root.insert(val) (in
	 * Base)). Then it checks if new_root is NULL (the root didn't split) if so it returns 1. If is !NULL (root split) it calls 
	 * Pnode's construct for new root and pass the aguments, root, new_root, and the first value in new_root. Then it assigns 
	 */ root to point to the node created by Pnode in the previous line. returns 0.
	int binsert(T element, int childmax, int parentmax);

	/* calls print in Base with root
	 */
	void print();
	
	/* calls save in Pnode on root
	 */
	bool save(std::string file_name);

	/* calls load in Pnode ???
	 */
	void load(std::string file_name);
};

template<typename T>
bool Btree<T>::save(std::string file_name) {
	std::ofstream ofs(file_name);
	if(root->is_child == false) {
		int offset = (std::static_pointer_cast<Pnode<T>>(root))->save(ofs);
		if(offset > -1) {
			cereal::BinaryOutputArchive sr(ofs);
			sr(offset);
			sr(root->is_child);
			return true;
		}
		else {
			exit(1);
		}
	}
	assert(root->child == true);
	int offset = (std::static_pointer_cast<Cnode<T>>(root))->save(ofs);
	if(offset > -1) {
		cereal::BinaryOutputArchive sr(ofs);
		sr(offset);
		sr(root->is_child);
		return true;
	}
	assert(offset != -1);
	return false;
}

template<typename T>
void Btree<T>::load(std::string file_name) {
	std::ifstream ifs(file_name);
	if(ifs.open) {
		
	}
	std::cout << "Cannot open file. Exiting \n";
	exit(1);
}

template<typename T>
Btree<T>::Btree(void) {}						// default constructor

template<typename T>
int Btree<T>::binsert(T element, int childmax, int parentmax) {
	if (root == NULL) {						// if btree has not been created, it creates the root of type cnode
//		if(std::is_class<T>::value) {
//			std::cout << std::is_class<T>::value << '\n';
//		}
		std::shared_ptr<Cnode<T>> croot = std::shared_ptr<Cnode<T>>();
		std::shared_ptr<Base<T>> create_root = std::make_shared<Cnode<T>>(element, childmax, parentmax);
		root = create_root;
		return 1;
	}
	assert(root != NULL);						// if btree has been created
	if(root->is_child == true) {					// if root is a cnode calls insert in Cnode  
		std::shared_ptr<Cnode<T>> newcnode = std::shared_ptr<Cnode<T>>();
		std::static_pointer_cast<Cnode<T>>(root)->cinsert(element, newcnode);
		if (newcnode != NULL) {					// If cnode split, this creates a new root of type Pnode
//			std::cout << "creating new Pnode root \n";
			newcnode->backward = std::static_pointer_cast<Cnode<T>>(root);
			root = std::static_pointer_cast<Base<T>>(std::make_shared<Pnode<T>>(element, 
				root, std::static_pointer_cast<Base<T>>(newcnode), childmax, parentmax));                         
//			std::cout << "immediately after spliting : " << std::static_pointer_cast<Pnode<T>>(root)->pointer_vector.size() << '\n';
//			std::cout << "root spilt \n";
			std::cout << "root spilt \n";
		}
//		std::cout << "after spliting : " << std::static_pointer_cast<Pnode<T>>(root)->pointer_vector.size() << '\n';
	}
	else {								// if root is a Pnode calls insert in Pnode
//		std::cout << "calling pinsert on root : " << std::static_pointer_cast<Pnode<T>>(root)->pointer_vector.size() << '\n';
		std::shared_ptr<Pnode<T>> newcnode = std::shared_ptr<Pnode<T>>();
		std::shared_ptr<Pnode<T>> pnode_root = std::static_pointer_cast<Pnode<T>>(root);
		pnode_root->pinsert(element, newcnode);
		if (newcnode != NULL) {					// if the root split creates a new root of type Pnode
			root = std::static_pointer_cast<Base<T>>(std::make_shared<Pnode<T>>(element, root, 
				newcnode, childmax, parentmax));                         
//			std::cout << "root spilt \n";
		}
	}
}

/******************************************
void Btree<T>::one_file_save(std::string file_name) {
	root->save_one_file(file_name);	
}
*******************************************/

template<typename T>
void Btree<T>::print() {						// calls appropriate print function 
	if (root == NULL) {
		return;
	}
	if(root->is_child == true) {
		std::static_pointer_cast<Cnode<T>>(root)->print_cnodes();
		return;
	}
	assert(root->is_child != true);
	std::static_pointer_cast<Pnode<T>>(root)->print_them();
}

template<typename T>
Btree<T>::~Btree() {
	std::cout << "destroying the balanced tree \n";
}

