// DiskvesionofBalancedTreenewer.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <type_traits>
#include <vector>
#include <string>
#include <memory>
#include <string>
#include <fstream>
#include <assert.h>
#include <memory>
#include "Base.hpp"
#include "Cnode.hpp"
#include "Pnode.hpp"
#include "Btree.hpp"
#include "Blanket.hpp"
#include <typeinfo>

int child_max;
int parent_max;
int min = 100;
int max = 400;

template<typename T>
void get_userinput(T& val, std::string print) {
	std::cout << print;
	std::cin >> val;
	std::cout << '\n';
}

template<typename T>
void get_child_max(T find_size) {
	std::cout << sizeof(find_size) << '\n';
	long double size = (sizeof(find_size)) / 64;
	std::cout << size << '\n';
	if (size > 400) {
		std::cout << "Object is too big to store \n" << "Exiting program";
		exit(1);
	}
	if (size < 100) {
		size = 100 / (find_size);
		std::cout << size << '\n';
	}
	child_max = (int)size;
	std::cout << "The child node will store a maximum of " << child_max << " elements \n";
}

int main() {
	int how_many = 15;
	/************************************
	get_userinput(how_many, "How many node do you want to store in the Btree? ");
	get_userinput(parent_max, "What's the maximum number of elements a parent node can hold? ");
	if (how_many <= 0 || parent_max <= 0) {
		exit(1);
	}
	**************************************/
	parent_max = 3;
//	Btree<std::shared_ptr<Blanket>> bt;	// change based on object type
//	Btree<int> bt;
	Btree<std::string> bt;
	std::string str = "hi there, how'u doing ?"; // change accroding to what is being inserted
	std::string element = str;
//	get_userinput(element, "Enter element : ");
//	get_child_max(element);
	how_many--; 
	child_max = 3;
//	std::vector<std::shared_ptr<Blanket>> blankets;
//	creating(blankets, how_many);
//	std::shared_ptr<Blanket> element = blankets[0];
//	blankets.clear();
//	srand(how_many);
//	int element = rand();
//	element->print_object();
	int n = bt.binsert(element, child_max, parent_max);
	if (n == 1) {
		std::cout << "Root was created \n";
	}
	int k = 1;
	while (how_many > 0) {
//		std::cout << "here's what is in the tree so far: \n";
		get_userinput(element, "Enter element : ");
//		element = blankets[k];
//		std::cout << k << '\n';
//		element->print_object();
//		srand(element); 
//		element = rand();
//		std::cout << "in main size of root : " << (std::static_pointer_cast<Pnode<int>>(bt.root))->pointer_vector.size() << '\n'; 
		bt.binsert(element, child_max, parent_max);
		/**************************************************************************************
		if(nroot->is_child == true) {
			std::shared_ptr<Cnode<std::string>> newroot = std::shared_ptr<Cnode<std::string>>();
			std::static_pointer_cast<Cnode<std::string>>(nroot)->cinsert(element, newroot);
			if (newroot != NULL) {
				std::cout << "croot->forward->val_vector[0] = " << croot->forward->val_vector[0] << '\n';
				newroot->backward = croot;
				std::cout << "newroot->backward->val_vector[0] = " << newroot->backward->val_vector[0] << '\n';
				std::cout << "current root is full \n";
				nroot = std::static_pointer_cast<Base<std::string>>(std::make_shared<Pnode<std::string>>(element, croot, 
					newroot, child_max, parent_max));
				std::cout << "root spilt \n";
				std::cout << nroot->val_vector[0] << '\n';
			}
		}
		else {
			std::cout << "using Pnode nroot \n";
			std::shared_ptr<Pnode<std::string>> newroot = std::shared_ptr<Pnode<std::string>>();
			std::shared_ptr<Pnode<std::string>> pnode_root = std::static_pointer_cast<Pnode<std::string>>(nroot);
			pnode_root->pinsert(element, newroot);
			if (newroot != NULL) {
				nroot = std::static_pointer_cast<Base<std::string>>(std::make_shared<Pnode<std::string>>(element, nroot, 						std::static_pointer_cast<Base<std::string>>(newroot), child_max, parent_max));
				std::cout << "root spilt \n";
				std::cout << nroot->val_vector[0] << '\n';
			}
		}
		else {
			std::shared_ptr<Pnode<int>> newroot = nroot->pinsert(element);
			if (newroot != NULL) {
				std::cout << "current root is full \n";
				nroot = std::make_shared<Pnode<int>>(element, froot, newroot, child_max, parent_max);
				std::cout << "root spilt \n";
			}
		}
		if (n == 1) {
			std::cout << "Element was insert somewhere in tree \n";
		}
		if (n == 0) {
			std::cout << "The root split and a new one was created \n";
		}
		**************************************************************************************/
		how_many--;
		k++;
	}
	bt.print();
//	blankets.clear();
	return 0;
}

